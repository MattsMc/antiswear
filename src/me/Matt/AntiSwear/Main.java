package me.Matt.AntiSwear;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {


	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		saveDefaultConfig();
		saveConfig();
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		for (String word : e.getMessage().split(" ")) {
			if (getConfig().getBoolean("enabled") == true) {
				if (getConfig().getBoolean("op-bypass") == true) {
					if (e.getPlayer().isOp()) {
					}
				} else if (getConfig().getStringList("banned-words").contains(
						word)) {
					e.setCancelled(true);
					e.getPlayer().sendMessage(
							ChatColor.DARK_RED
									+ "[AntiSwear] "
									+ ChatColor.RED
									+ getConfig().getString(
											"message-to-swearererer"));
				}
			} else {
			}
		}

	}
}
